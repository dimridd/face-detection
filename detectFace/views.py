# import the necessary packages

from core.utils import generate_image_id, generate_request_id, generate_token_id

# Third party imports
from rest_framework import status
from rest_framework.decorators import api_view
from django.http import HttpResponse
from rest_framework.views import APIView
import json
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
import numpy as np
import imutils
import cv2
import os
import copy
from .serializers import UploadImageSerializer
from .models import Post, ServiceResponse

# Django imports
from django.conf import settings
import time

SUCCESS = 'success'
DELETE_SUCCESS = 'deleted'

"""
create recognizer here
"""

# load the face detector cascade and smile detector CNN
protoPath = os.path.join(settings.ML_ROOT, "models", "deploy.prototxt")
modelPath = os.path.join(settings.ML_ROOT, "models", "res10_300x300_ssd_iter_140000.caffemodel")

detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)


class FaceDetect(APIView):

    def timer(self, start, end):
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)

        return "{:0>2}:{:05.2f} min".format(int(minutes), seconds)

    start = time.time()

    # emptying save directory
    save_path = os.listdir(os.path.join(settings.ML_ROOT, "saves"))
    if len(save_path):
        for file in save_path:
            os.remove(os.path.join(settings.ML_ROOT, "saves", file))

    # permission_classes = (IsAuthenticated, )

    serializer_class = UploadImageSerializer

    def post(self, request):
        image = request.POST["name_of_file"]

        # image = os.path.join(settings.MEDIA_ROOT.split("/")[:], "media", data_['image_file'].split("/")[-1])
        face_counter = 0

        # image = (os.path.join(settings.MEDIA_ROOT, "images", data_['image_file'].split("/")[-1]))
        img_nm = image

        # resize the frame to have a width of 600 pixels (while
        # maintaining the aspect ratio), and then grab the image
        # dimensions
        image = cv2.imread(image)

        image = imutils.resize(image, width=600)
        (h, w) = image.shape[:2]

        # construct a blob from the image
        imageBlob = cv2.dnn.blobFromImage(
            cv2.resize(image, (300, 300)), 1.0, (300, 300),
            (104.0, 177.0, 123.0), swapRB=False, crop=False)

        # apply OpenCV's deep learning-based face detector to localize
        # faces in the input image
        detector.setInput(imageBlob)
        detections = detector.forward()

        faces = []
        faces_dict = {}

        ans = {}
        # loop over the detections
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with
            # the prediction
            confidence = detections[0, 0, i, 2]

            # filter out weak detections
            if confidence > 0.5:
                # compute the (x, y)-coordinates of the bounding box for
                # the face
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                # extract the face ROI
                face = image[startY:endY, startX:endX]
                face_copy = copy.deepcopy(face)

                (fH, fW) = face.shape[:2]

                # ensure the face width and height are sufficiently large
                if fW < 20 or fH < 20:
                    continue

                text = "Face {}".format(face_counter + 1)
                y = startY - 10 if startY - 10 > 10 else startY + 10
                cv2.rectangle(image, (startX, startY), (endX, endY),
                              (0, 100, 0), 2)
                cv2.putText(image, text, (startX, y),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 100, 0), 2)

                outputFile = os.path.join(settings.ML_ROOT, "saves", "detect_{}.jpg".format(face_counter + 1))

                if not os.path.exists(outputFile) and face.size != 0:
                    cv2.imwrite(outputFile, face_copy)

                faces.append({"face {}".format(face_counter + 1): [{
                    "face_token": generate_token_id(),
                    "detect": {"top": str(startX),
                               "left": str(startY),
                               "width": str(endX),
                               "height": str(endY),
                               }}, {"face no": face_counter + 1}]})
                face_counter += 1

                cv2.imwrite(os.path.join(settings.ML_ROOT, "image", "detect.jpg"), image)

        ans["faces"] = faces
        with open(os.path.join(settings.ML_ROOT, "files", "file.json"), "w") as fptr:
            json.dump(ans, fptr)

        image_name = (img_nm.split("/")[-1].split(".")[0])

        get_object = ServiceResponse.objects.filter(image_name=image_name).first()
        if get_object:
            ans["image_id"] = get_object.image_id
            ans["request_id"] = str(get_object.request_id)
        else:
            image_id = generate_image_id()
            ServiceResponse.objects.get_or_create(
                image_name=image_name,
                image_id=image_id,
            )

            get_object = ServiceResponse.objects.get(image_name=image_name)
            get_object.request_id = generate_request_id() + str(get_object.uuid)

            get_object.save()

            ans["image_id"] = image_id
            ans["request_id"] = str(get_object.request_id)

        end = time.time()

        ans["time_used"] = self.timer(self.start, end)

        return HttpResponse(json.dumps(ans), content_type="application/json")


@api_view(['DELETE', ])
def DeleteImagesDetail(request):
    try:
        post = Post.objects.all()
    except Post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if len(post) > 1:
        if request.method == 'DELETE':
            for entry in range(len(post) - 1, 0, -1):
                img_path = os.path.join(settings.MEDIA_ROOT, str(post[entry].image_file))
                os.remove(img_path)
                operation = post[entry].delete()

            data = {}
            if operation:
                data[SUCCESS] = DELETE_SUCCESS

            return Response(data=data)
