# Django imports
from django.utils.translation import ugettext_lazy as _
from django.db import models

# Local imports
from django.core.files import File
import os
import urllib
# from service.core.validators import validator_ascii

# Third party imports
from model_utils.models import TimeStampedModel
import uuid

# Create your models here.


class Post(TimeStampedModel):

    title = models.CharField(
        _("Title"), max_length=255, blank=True, null=True
    )
    image_file = models.ImageField(
        _("Image"), null=True, blank=True, upload_to="images/"
    )

    def __str__(self):
        return self.title


class ServiceResponse(TimeStampedModel):

    image_name = models.CharField(
        _("Image Name"), max_length=255, blank=True, null=True
    )
    uuid = models.UUIDField(max_length=100, blank=True, unique=True, default=uuid.uuid4)
    request_id = models.CharField(
        _("Request ID"), max_length=255, blank=True, null=True
    )

    image_id = models.CharField(
        _("Image ID"), max_length=255, blank=True, null=True
    )

    def __str__(self):
        return self.image_name
