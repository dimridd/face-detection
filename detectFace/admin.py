from django.contrib import admin

from .models import Post, ServiceResponse
# Register your models here.

admin.site.register(Post)
admin.site.register(ServiceResponse)
