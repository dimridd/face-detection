from rest_framework import serializers

from .models import Post


class UploadImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.image_file = validated_data.get("image_file", instance.image_file)
        if instance.title is not None and instance.image_file is not None:
            instance.save()

        return instance
