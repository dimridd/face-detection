from django.urls import path
from .views import FaceDetect, DeleteImagesDetail

app_name = 'detectFace'

urlpatterns = [
    path('det_face/', FaceDetect.as_view(), name='det_face'),
    path('delete/', DeleteImagesDetail, name='delete-image'),
]
