# Std imports
import string
from random import choices

# Django imports
from django.conf import settings


def generate_request_id():
    return "".join(choices(string.digits, k=10)) + ","


def generate_image_id():
    return ''.join(choices(string.ascii_letters + string.digits + '+/', k=22)) + "=="


def generate_token_id():
    return ''.join(choices(string.ascii_letters + string.digits, k=32))
